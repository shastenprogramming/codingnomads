# Save the user input options you allow e.g. in a set that you can check against when your user makes a choice.

# Create an inventory for your player, where they can add and remove items.

# Players should be able to collect items they find in rooms and add them to their inventory.

# If they lose a fight against the dragon, then they should lose their inventory items.

# Add more rooms to your game and allow your player to explore.

# Some rooms can be empty, others can contain items, and yet others can contain an opponent.

# Implement some logic that decides whether or not your player can beat the opponent depending on what items they have in their inventory

# Use the random module to add a multiplier to your battles, similar to a dice roll in a real game. This pseudo-random element can have an effect on whether your player wins or loses when battling an opponent.

# Once you've got a working version done, show it again to your friends and family and let them give it another spin. Feel free to share a link to your game on the forum in the thread on Command-Line Games.


import sys
import random as ran

def main():
    player_lives = 3
    player_score = 0
    player_items = []
    
    def player_items():
        pass

    def dice_roll():
        pass 
    
    def player_items():
        player_items = []
    
    def enemies():
        pass

    def room():
        choice = input("What room would you like to enter (N)orth, (S)outh, (E)ast, or (W)est? ").upper()
        
        if choice == "N":
            north_room() 
        elif choice == "S": 
            south_room()
        elif choice == "W":
            west_room()
        else:
            east_room() 
    
    def east_room():
        pass

    def west_room():
        player_combat = input("There is a dragon. Do you want to (L)eave or (S)tay and fight? ").upper()
        player_combat == True
        weapons(player_items)
    
    def north_room():
        pass
    
    def south_room():
        pass
    
    def staff():
        pass
    #if player has a staff, they will get swiped once before killing the dragon. 
    
    def sword():
        print("Congrats, you just defeated the dragon.")
        quit()
    
    
    def no_weapon():
        dragon_swipe = True
        health()
    
    
    
    def lives():
        nonlocal player_lives
        while player_lives > 1:
            player_lives -= 1    
            print(f"You just lost a life. You now have {player_lives} lives left.")
            room()    
        else:
            player_lives == 0
            print("You lose.")
            quit()
    
    def health():
    
        player_health = 6
        player_health = int(player_health)
        while player_health > 2:
            player_health -= 2
            print(f"You just got swiped by the dragon. You now have {player_health} health.")
            player_combat = input("Do you want to (L)eave or (S)tay and fight? ").upper()
            if player_combat == "L":
                lives()
        else:
            lives()
    
    def weapons(player_items):
        if player_items == "sword":
            sword()
        elif player_items != "sword" and player_items == "staff":
            staff()
        elif player_items != "sword" and player_items != "staff":
            no_weapon()
        
    def quit():
        play_again = input("Game Over. Would you like to play again (Y/N?)").upper()
        if play_again == "Y":
            room()
        else:
            sys.exit("Thanks for playing.")
    room()
main()