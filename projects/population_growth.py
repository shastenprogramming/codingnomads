#list current population
population = 380123456

#create variables to figure out how many minutes in 3 years
years = 3
days = 365
hours = 24
minutes = 60

# ascertain how many people are born every minute
born_per_minute = 10
#ascertain how many people die every minute
die_per_minute = 5
#ascertain how many people immigrates every 2 minutes
immigrates_two_minutes = 3
#figure out how many people will be born in 3 years
people_born = int(born_per_minute * minutes * hours * days * years)
#figure out how many people will die in 3 years
people_died = int(die_per_minute * minutes * hours * days * years)
#figure out how many people will immigrate in 3 years
people_immigrated =(int(immigrates_two_minutes * minutes / 2) * hours * days * years) 

#add it all up. 
population_three_years = (population + people_born - people_died + people_immigrated)
print("The population is projected to be", population_three_years, "in three years!")

