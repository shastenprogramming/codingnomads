# ask user what kilometers for the upcoming trip
trip = int(input("How many kilometers is the upcoming trip projected to be? "))
# car economy liters per kilometer
economy = float(input("How many liters per kilometer does your car get? "))
#price per liter
pesos = float(input("How many pesos per liter does gasoline cost? "))
# Total projected cost of the trip
total_projected_cost = (trip / economy) * pesos
print(f"Your projected cost for the trip will be", {total_projected_cost})
