# Define a function called `write_letter()` that takes as input a `name`
# and a `text` argument. In the body of the function, create a greeting
# message with the `name` input, as well as a goodbye message that uses
# the `name` again. Combine that with the input `text` to return a
# complete `letter`.

def write_letter():
    name = input("Name: ")
    text = print(f"""Hi {name}, I just wanted to say how 
            how nice it was to see you the other night
            at the BBQ. We will be in touch shortly {name}.
            Respectfully, Bill""")

write_letter()
