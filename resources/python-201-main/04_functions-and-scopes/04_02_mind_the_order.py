# Something is wrong with the way you're calling the function below.
# Can you fix it and survive?
step_1 = "JUMP!"
step_2 = "Take your parachute"
def skydive(step_1, step_2):
    print("For your own safety, please follow the instructions carefully:")
    print(f"1. {step_1}")
    print(f"2. {step_2}")

skydive(step_1, step_2)
