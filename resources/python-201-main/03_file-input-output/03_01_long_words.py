# Write a program that reads in `words.txt` and prints only the words
# that have more than 20 characters (not counting whitespace).
#f = open("words.txt", "r")

with open("words.txt", "r") as file:
    for line in file:
        for word in line.split():
            if len(word) >= 20:
                print(word)




