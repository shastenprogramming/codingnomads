# Read in all the words from the `words.txt` file.
# Then find and print:

#def main():
#    with open("words.txt", "r") as file:
#        words = file.read()
#        sorted_words = sorted(words, key=len, reverse = True)
#        print("The number of words in the list is : %s." % (len(words),
#        print("The shortest words in the list are : %s." % (sorted_words[0],
#        print("The longest words in the list are : %s." % (sorted_words(words[-1],
#main()
words = []
def longest_word(words):
    with open('words.txt', 'r') as file:
            words = file.read().split()
    max_len = len(max(words, key=len))
    return [word for word in words if len(word) == max_len]
print("The longest words are: ", longest_word(words))

def shortest_words(words):
    with open('words.txt', 'r') as file:
            words = file.read().split()
    min_len = len(min(words, key=len))
    return [word for word in words if len(word) == min_len]
print("The shortest words are: ", shortest_words(words))

def number_of_words():
    with open("words.txt", "r") as file:
        words = file.read().split()
        print("The number of words are: ", len(words))
number_of_words()

