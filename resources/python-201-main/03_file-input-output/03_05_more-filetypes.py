# Adapt your file counter script so that it records more different file types
# in your CSV file. Remember that the format of your output needs to be
# consistent across multiple runs of your script. This means you'll need to
# make a compromise and choose which file types you want to record beforehand.

from pathlib import Path

data_path = Path("/home/tuxer/Desktop")

with open(data_path.joinpath("input.txt"), "w") as file_out:
    print(file_out.out())

def file_type():
    counter = 0
    print(f"Choose a file type: ")
    fileType = input("""        1. docx
        2. iso
        3. jpeg
        4. mp3
        5. mp4
        6. ogg
        7. pdf
        8. png
        9. svg
        10. wav
        """)
    if fileType == "1":
        /home/tuxer/Desktop.suffix = '.docx' 
    elif fileType == "2":
        data_path.suffix = '.iso' 
    elif fileType == "3":
        data_path.suffix = '.jpg'
    elif fileType == "4":
        data_path.suffix = '.mp3'
    elif fileType == "5":
        data_path.suffix = '.mp4'
    elif fileType == "6":
        data_path.suffix = '.ogg'
    elif fileType == "7":
        data_path.suffix = '.pdf'
    elif fileType == "8":
        data_path.suffix = '.png'
    elif fileType == "9":
        data_path.suffix = '.svg'
    else:
        data_path.suffix = '.wav'

    for x in fileType:
        counter += 1
        return counter
file_type()
    