import copy
# Read in 10 numbers from the user
# Place all 10 numbers into an list in the order they were received.
# Print out the second number received, followed by the 4th, 
# then the 6th, then the 8th, then the 10th.
# Then print out the 9th, 7th, 5th, 3rd, and 1st number:
#
# Example input:  1,2,3,4,5,6,7,8,9,10
# Example output: 2,4,6,8,10,9,7,5,3,1
#list_a = [2,4,6,8,10]
#list_b = [1,3,5,7,9]
#
#list_c = (list_a, list_b[::-1])
#print(list_c[0] + list_c[1])
list_a = []
list_b = []
list_c = []
for i in range(10):
    var = input("Enter a number: ")
    list_a.append(var) 
    
list_b = list_a[1:10:2]
list_c = list_a[-2::-2]
print(list_b + list_c)
