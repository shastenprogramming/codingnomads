# Convert a string to a tuple and print out the result.
# What do you see?
# What happens if you try to iterate over your tuple of characters?
# Do you notice any difference to iterating over the string?

string = "codingnomads"
string = tuple()
print(type(string))

""" When you iterate over the tuple, from that point on the variable is 
going to be that specific datatype until otherwise specified. Sometimes,
it could be difficult to know what that datatype is, especially in the 
event that the change occurs in a sequence."""
