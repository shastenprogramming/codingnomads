# The import below gives you a new random list of numbers,
# called `randlist`, every time you run the script.
#
# Write a script that takes this list of numbers and:
#     - sorts the numbers
#     - stores the numbers in tuples of two in a new list
#     - prints each tuple
#
# If the list has an odd number of items,
# add the last item to a tuple together with the number `0`.
#
# Note: This lab might be challenging! Make sure to discuss it 
# with your mentor or chat about it on our forum.

from resources import randlist
list_of_tuples = []
numbers = randlist
sorted_numbers = sorted(numbers)
#print(numbers)
for pair_of_nums in sorted_numbers:
    while len(pair_of_nums) % 2 != 0:
        list_of_tuples.append(pair_of_nums)
    else:
        pass

for pair_of_nums in list_of_tuples:
    print(pair_of_nums)



# Write your code below here

