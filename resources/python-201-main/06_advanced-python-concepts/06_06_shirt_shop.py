# Using a list comprehension, create a *cartesian product* (google this!)
# of the given lists. Then open up your online shop ;)
import itertools

colors = ["neon orange", "spring green"]
sizes = ["S", "M", "L"]

for e in itertools.product(colors, sizes):
    print(set(list(e)))
