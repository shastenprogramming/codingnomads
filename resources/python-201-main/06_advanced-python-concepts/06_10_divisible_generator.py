# Create a Generator that loops over the given range and prints out only
# the items that are divisible by 1111.

#nums = range(1, 1000000)

gen = [nums % 1111 == 0 for nums in range(1, 1000000)]
for i in gen:
    print(i)

