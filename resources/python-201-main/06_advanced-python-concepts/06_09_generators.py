# Demonstrate how to create a generator object.
# Print the object to the console to see what you get.
# Then iterate over the generator object and print out each item.

gen = (i * 3 for i in range(101))
for i in gen:
    print(i)
