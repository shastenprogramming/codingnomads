import random
import sys

def play_game():
    user_input = int(input("Choose a number between 1 and 3: "))
    c = random.randint(1,3)
    while c == user_input:
        print("It's a tie.")
        play_again()
    else:
        breakpoint()
        if c == 1:
            if user_input == 2:
                print("Paper covers rock! You win!")
                #breakpoint()
                play_again()
            elif user_input == 3:
                print("Scissor's crush rock! You lose!")
                play_again()
        elif c == 2:
            if user_input == 1:
                print("Paper covers rock. You lose!")
                play_again()
            elif user_input == 3:
                print("Scissor cuts paper. You win!")
                play_again()
        elif c == 3:
            if user_input == 2:
                print("Scissors cuts paper. You lose!")
                play_again()
            elif user_input == 1:
                print("Rock crushes scissors. You win!")    
                play_again()

def play_again():
    play_again = input("Do you want to play again? (Y/N)").upper()
    if play_again == "Y":
        play_game()
    else:
        sys.exit()
play_game()