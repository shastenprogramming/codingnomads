# Extract four words of edible food items from the sentence below.
# Use only string slicing to pick them out!
# Feel free to use pen and paper to number the indices
# and find the locations quicker.
#
# What dish can you make from these ingredients? :)

s = "They grappled with their leggins before going to see the buttercups flourish."
#print(len(s)) #77

#First edible grape
s1 = s[5:12]
e1 = s1[0:4]
print(e1 + s1[6])

#Second edible leg
s1 = s[25:28]
print(s1)

#Third edible butter
s1 = s[57:63]
print(s1)

#Fourth edible flour
s1 = s[68:73]
print(s1)