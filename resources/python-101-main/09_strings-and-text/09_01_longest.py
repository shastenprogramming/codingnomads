# Which of the following strings is the longest?
# Use the len() function to find out.
list = ["Donaudampfschifffahrtsgesellschaftskapitänskajütentürschnalle", "Megszentségteleníthetetlenségeskedéseitekért", "Lentokonesuihkuturbiinimoottoriapumekaanikkoaliupseerioppilas", "%8Ddb^ca<*'{9pur/Y(8n}^QPm3G?JJY}\(<bCGHv^FfM}.;)khpkSYTfMA@>N"]
longest_german_word = "Donaudampfschifffahrtsgesellschaftskapitänskajütentürschnalle"
longest_hungarian_word = "Megszentségteleníthetetlenségeskedéseitekért"
longest_finnish_word = "Lentokonesuihkuturbiinimoottoriapumekaanikkoaliupseerioppilas"
strong_password = "%8Ddb^ca<*'{9pur/Y(8n}^QPm3G?JJY}\(<bCGHv^FfM}.;)khpkSYTfMA@>N"

# I'm not this advanced yet, but I did see this example and worked into my code.
# assigns longest word to the appropriate variable longest_word
longest_word = max(list, key = len)

#if statement to print the longest word through a process of elimination.
if longest_word == longest_german_word:
    print('German has the longest word.')
elif longest_word == longest_hungarian_word:
    print('Hungarian has the longest word.')
elif longest_word == longest_finnish_word:
    print('Finnish has the longest word.')
else:
    print('My Strong and complicated password has the longest amount of characters.')

animal = "aardvark"
print(len(animal))
