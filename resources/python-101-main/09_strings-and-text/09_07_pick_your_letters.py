# Use string indexing and string concatenation
# to write the sentence "we see trees" only by picking
# the necessary letters from the given string.

word = "tweezers "

#s1 = word[1:3]
#print(s1)
#s2 = word[7:2:-2]
#print(s2)
#s3 = word[0:7:6]
#print(s3)
#s4 = word[2:4]
#print(s4)
#s5 = word[7]
#print(s5)
#
print(word[1:3], word[7:2:-2], word[0:7:6] + word[2:4] + word[7])