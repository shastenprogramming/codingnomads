# Fahrenheit to Celsius:
# ----------------------
# Write the necessary code to convert a degree in Fahrenheit
# to Celsius and print it to the console. Use variable names!

F = int(input("What is the temperature Fahrenheit? "))
C = (F - 32) * 5 / 9
print(F, "degrees Fahrenheit equals", C, "degrees Celsius.")
