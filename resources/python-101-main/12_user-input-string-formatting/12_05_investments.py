# Take in the following three values from the user:
# 1. investment amount
# 2. interest rate in percentage
# 3. number of years to invest
#
# Calculate the future values and print them to the console.

monthly_investment = int(input("How much do you want to invest each month? "))
projected_monthly_earnings = int(input("What do want to earn every month? "))
annualized_return = (projected_monthly_earnings / monthly_investment) ** ((1/3) -1)
percentage_earnings = annualized_return 
percentage_earnings = int(percentage_earnings)
print("Your expected earnings should be", percentage_earnings, "percent.")
print("Your expected gains should be", projected_monthly_earnings * 36, "dollars from a total investment of", monthly_investment * 36, "over 36 months." )


