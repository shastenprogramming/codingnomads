# Write a script that takes three strings from the user
# and prints the longest string together with its length.
#
# Example Input:
#     hello
#     world
#     greetings
#
# Example Output:
#     9, greetings

s1 = input("Enter in some text: ")
s2 = input("Enter in some more text: ")
s3 = input("Enter in even more text: ")
list = [s1, s2, s3]
longest_string = max(list, key = len)
len_longest_string = len(longest_string)
print(len_longest_string, longest_string)
