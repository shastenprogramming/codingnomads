# Ask the user to input their name. Then print a nice welcome message
# that welcomes them personally to your script.
# If a user enters more than one name, e.g. "firstname lastname",
# then use only their first name to overstep some personal boundaries
# in your welcome message.

user = str(input("ENTER YOUR NAME: "))
user = user.upper()
name = user.split()
first_name = name[0]
print("Hi", first_name, "how are you doing?")

