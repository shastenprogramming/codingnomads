# Write a script that takes a string input from a user
# and prints a total count of how often each individual vowel appeared.

words = input("Enter a series of words or a sentence: ")
print("A:", words.count('a'))
print("E:", words.count('e'))
print("I:", words.count('i'))
print("O:", words.count('o'))
print("U:", words.count('u'))



