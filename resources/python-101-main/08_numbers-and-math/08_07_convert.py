# Demonstrate how to:
# -------------------
# 1) Convert an int to a float
# 2) Convert a float to an int
# 3) Perform division using a float and an int.
# 4) Use two variables to perform a multiplication.
#
# What information is lost during which conversions?

x = float(3)
print(x)

y = int(4.5)
print(y)

z = 6.6 / 2
print(z)

print(x * y)



