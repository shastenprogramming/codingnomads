# Write code to display the area and perimeter
# of a rectangle that has a width of 2.4 and a height of 6.4.

Height = 6.4
Width = 2.4

Perimeter = float(Width * 2.0) + float(Height * 2.0)
Area = float(Height * Width)
print("Perimeter:", Perimeter)
print("Area:", Area)