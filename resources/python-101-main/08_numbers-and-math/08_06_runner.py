# If a runner runs 10 miles in 30 minutes and 30 seconds,
# What is their average speed in kilometers per hour?
# (Tip: 1 mile = 1.6 km)


# ascertain the pace per mile
# runner runs 1 mile every 3.03 minutes
# multiply by 60

miles = 30.5/10
print(miles)

miles_per_hour = 60 / miles
print("Runner runs", miles_per_hour, "per hour.")

kilometers_per_hour = miles_per_hour * 1.6
print("Runner runs", kilometers_per_hour, "per hour")




