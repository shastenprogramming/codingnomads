# Write the necessary code calculate the volume and surface area
# of a cylinder with a radius of 3.14 and a height of 5.
# Print out the result.

radius = 3.14
height = 5.0

s_area = (2 * (3.14 * (3.14 ** 2))) + (2 * (3.14 * 3.14 * (5)))
volume = s_area * height

print("Surface area is ", s_area)
print("volume is ", volume)


