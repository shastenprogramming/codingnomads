# Fix the variable assignment shown below.
# Can you see why using the multiple variable assignment can be tricky?
# Declared like this, it's easy to mix which is which.

dreams, profession = "flying", "programming"
print(dreams, profession)

#It's only tricky if you use too many variables with too many strings or values.

