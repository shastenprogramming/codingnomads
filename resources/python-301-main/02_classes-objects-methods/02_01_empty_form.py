# A good way to think about how classes are blueprints of objects is to think of
# an empty form, for example one that you would get at a doctor's office.
# The empty form contains all the placeholders that define what information
# you need to fill to complete the form. If you fill it correctly, then you've
# successfully instantiated a form object, and your completed form now holds
# information that is specific to just you.
# Another patient's form will follow the same blueprint, but hold different info.
# You could say that every patient's filled form instance is part of the same
# empty form blueprint class that the doctor's office provided.
#
# Model such an application form as a Python class below, and instantiate
# a few objects from it.

class Form():
    def __init__(self, name, phone_number, medical_disease, medical_history, symptoms, allergies, other):
        self.name = name
        self.phone_number = phone_number
        self.medical_disease = medical_disease
        self.medical_history = medical_history
        self.symptoms = symptoms
        self.allergies = allergies
        self.other = other

    def __str__(self):
        return f"Patient {self.name} Phone Number: {self.phone_number} has the condition {self.medical_disease} with a history of {self.medical_history} that exhibit symptoms of {self.symptoms}. He also has allegies to {self.allergies}, and {self.other}."

mike = Form('Mike', '346-645-7564', 'kidney disease and diabetes', 'high blood pressure', 'chronic fatigue', 'walnuts', 'yellowish color skin')
john = Form('John', '555-123-4567', 'borderline diabetic', 'low iron levels', 'chronic fatigue', 'eggs', 'overweight')
print(mike)
print(john)